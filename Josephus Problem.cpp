#include <iostream>
using namespace std;
struct Node
{
	int data;
	Node* next;
};

int nodeCounter(Node* head);
Node* lastNode(Node* head);
void addNodeToEnd(Node* head, int data);
Node* deleteNode(Node* head, int x);
Node* executioner(Node* head, int step);
void printList(Node* head);

void main()
{
	do{
		cout << "-------------------------------------------" << endl;
		Node* head = new(Node);
		head->data = 1;
		head->next = head;
		int seats;
		int steps;
		cout << "Enter Number of Seats: "; cin >> seats;
		cout << "Enter Steps: "; cin >> steps;
		for (int i = 2; i <= seats; i++)
		{
			addNodeToEnd(head, i);
		}

		printList(head);
		cout << endl;
		head = executioner(head, steps);
		cout << endl;
		cout << head->data << " Wins!" << endl;
	} while (true);
}

int nodeCounter(Node* head)
{
	int count = 1;
	Node* p = head;
	while (p->next != head)
	{
		count++;
		p = p->next;
	}
	return(count);
}

Node* lastNode(Node* head)
{
	Node* p = head;
	while (p->next != head)
	{
		p = p->next;
	}
	return p;
}

void addNodeToEnd(Node* head, int data)
{
	if (head == NULL)
	{
		Node* newNode = new (Node);
		newNode->data = data;
		newNode->next = newNode;
	}
	else
	{
		Node* p = head;
		while (p->next != head)
		{
			p = p->next;
		}
		Node* newNode = new(Node);
		newNode->data = data;
		newNode->next = head;
		p->next = newNode;
	}
}

Node* deleteNode(Node* head, int x)
{
	if (head->data == x)
	{
		if (head->next == head)
		{
			delete head;
			return nullptr;
		}
		Node* p = lastNode(head);
		p->next = head->next;
		delete head;
		return p->next;
	}
	Node* q = head;
	Node* p = head->next;
	do
	{
		if (p->data == x)
		{
			q->next = p->next;
			delete p;
			break;
		}
		q = p;
		p = p->next;
	} while (p != head);

	return head;
}

Node* executioner(Node* head, int step)
{
	if (step == 1)
	{
		if (nodeCounter(head) > 2)
		{
			Node* p = head;
			Node* q = new (Node);
			Node* r = new (Node);
			while (p->next != p)
			{
				q = p->next;
				r = p->next->next;
				p->next = r;
				cout << q->data << " Killed!" << endl;
				delete q;
				p = r;
			}
			return p;
		}
		else
		{
			if (head->next != head)
			{
				head->next = head;
				delete head->next;
				return head;
			}
			else
			{
				return head;
			}
		}
	}
	else
	{
		if (nodeCounter(head) > 2)
		{
			Node* klr = head;

			while (nodeCounter(klr) != 1)
			{
				Node* pre = klr;
				Node* kld = klr;
				for (int i = 0; i < step; i++)
				{
					pre = kld;
					kld = kld->next;
				}
				cout << kld->data << " Killed!" << endl;
				klr = kld->next;
				pre->next = klr;
				delete kld;
			}
			return klr;
		}
		else
		{
			if (head->next != head)
			{
				for (int i = 0; i < step; i++)
				{
					head = head->next;
				}
				Node* p = head->next;
				head->next = head;
				cout << p->data << " Killed!" << endl;
				delete p;
				return head;
			}
			else
			{
				return head;
			}
		}
	}
}

void printList(Node* head)
{
	Node* p = head;
	while (p->next != head)
	{
		cout << p->data << endl;
		p = p->next;
	}
	cout << p->data << endl;
}